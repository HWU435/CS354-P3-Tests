# CS354 P3 Tests

A collection of tests for the Memory Allocator project (P3). 

To use: copy the test file to the drivers directory of your project and re-run "make"

Note: Because this project was fairly open ended, our individual implementations probably vary a decent amount. Therefore I have left out certain assert statements in these tests because there is a decent chance they would not work. I recommend using a combination of gdb and the print heap function to make sure that the tests are being executed correctly.
